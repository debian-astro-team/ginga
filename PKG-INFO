Metadata-Version: 2.1
Name: ginga
Version: 5.2.0
Summary: A scientific image viewer and toolkit
Home-page: https://ejeschke.github.io/ginga/
Author: Ginga Maintainers
Author-email: eric@naoj.org
License: BSD
Keywords: scientific,image,viewer,numpy,toolkit,astronomy,FITS
Classifier: Intended Audience :: Science/Research
Classifier: License :: OSI Approved :: BSD License
Classifier: Operating System :: MacOS :: MacOS X
Classifier: Operating System :: Microsoft :: Windows
Classifier: Operating System :: POSIX
Classifier: Programming Language :: C
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: 3
Classifier: Topic :: Scientific/Engineering :: Astronomy
Classifier: Topic :: Scientific/Engineering :: Physics
Requires-Python: >=3.8
Description-Content-Type: text/plain
License-File: LICENSE.txt
Requires-Dist: numpy>=1.21
Requires-Dist: qtpy>=2.0.1
Requires-Dist: astropy>=5.0
Requires-Dist: puremagic>=1.28
Requires-Dist: pillow>=9.2
Requires-Dist: pyyaml>=6.0
Requires-Dist: tomli>=2.0.1; python_full_version < "3.11.0a7"
Requires-Dist: packaging>=23.1
Provides-Extra: recommended
Requires-Dist: scipy>=0.18.1; extra == "recommended"
Requires-Dist: matplotlib>=3.8; extra == "recommended"
Requires-Dist: opencv-python-headless>=4.5.4; extra == "recommended"
Requires-Dist: exifread>=2.3.2; extra == "recommended"
Requires-Dist: astroquery>=0.3.5; extra == "recommended"
Requires-Dist: python-dateutil>=2.8.2; extra == "recommended"
Requires-Dist: photutils; extra == "recommended"
Provides-Extra: test
Requires-Dist: attrs>=19.2.0; extra == "test"
Requires-Dist: pytest-astropy-header; extra == "test"
Requires-Dist: pytest-doctestplus; extra == "test"
Requires-Dist: pytest-remotedata; extra == "test"
Requires-Dist: scipy; extra == "test"
Requires-Dist: photutils; extra == "test"
Provides-Extra: docs
Requires-Dist: sphinx; extra == "docs"
Requires-Dist: sphinx-astropy; extra == "docs"
Requires-Dist: sphinx_rtd_theme; extra == "docs"
Provides-Extra: gtk3
Requires-Dist: pycairo; extra == "gtk3"
Requires-Dist: pygobject>=3.48.1; extra == "gtk3"
Provides-Extra: gtk4
Requires-Dist: pycairo; extra == "gtk4"
Requires-Dist: pygobject; extra == "gtk4"
Provides-Extra: qt5
Requires-Dist: PyQt5; extra == "qt5"
Provides-Extra: qt6
Requires-Dist: PyQt6; extra == "qt6"
Provides-Extra: pyside2
Requires-Dist: PySide2; extra == "pyside2"
Provides-Extra: pyside6
Requires-Dist: PySide6; extra == "pyside6"
Provides-Extra: tk
Requires-Dist: pycairo; extra == "tk"
Provides-Extra: web
Requires-Dist: tornado; extra == "web"
Requires-Dist: pycairo; extra == "web"

Ginga is a toolkit designed for building viewers for scientific image
data in Python, visualizing 2D pixel data in numpy arrays.
The Ginga toolkit centers around an image display class which supports
zooming and panning, color and intensity mapping, a choice of several
automatic cut levels algorithms and canvases for plotting scalable
geometric forms.  In addition to this widget, a general purpose
'reference' FITS viewer is provided, based on a plugin framework.

Ginga is distributed under an open-source BSD licence. Please see the
file LICENSE.txt in the top-level directory for details.
