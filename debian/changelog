ginga (5.2.0-2) unstable; urgency=medium

  * Team upload.
  * Add additional dependency on python3-pyqt5.qtsvg as it is imported in the
    module.
  * Add build-dependencies on pyqt5 packages for build-time tests (Closes:
    #1089950).

 -- Stuart Prescott <stuart@debian.org>  Wed, 08 Jan 2025 00:40:23 +1100

ginga (5.2.0-1) unstable; urgency=medium

  * New upstream version 5.2.0
  * Rediff patches
  * New build dependency python3-puremagic

 -- Ole Streicher <olebole@debian.org>  Thu, 24 Oct 2024 16:38:27 +0200

ginga (5.1.0-3) unstable; urgency=medium

  * 'd/clean': Add missing that fixes the failure of source build after
    successful build.
  * 'd/control':
    - Add myself to 'Uploaders'.
    - Remoce 'Build-Depends' for 'python3-pkg-resources' on 'ginga' package.
      (Closes: #1083416)
  * 'd/watch': Use version 4.

 -- Phil Wyett <philip.wyett@kathenas.org>  Sat, 19 Oct 2024 11:39:06 +0100

ginga (5.1.0-2) unstable; urgency=medium

  * Adjust Lorentz1D FWHM result to be compatible with astropy v6.1.3

 -- Ole Streicher <olebole@debian.org>  Sat, 31 Aug 2024 19:50:20 +0200

ginga (5.1.0-1) unstable; urgency=medium

  * New upstream version 5.1.0
  * Rediff patches
  * Push Standards-Version to 4.7.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Fri, 31 May 2024 15:48:48 +0200

ginga (5.0.1-1) unstable; urgency=medium

  * New upstream version 5.0.1
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Tue, 09 Apr 2024 15:09:39 +0200

ginga (5.0.0-1) unstable; urgency=medium

  * New upstream version 5.0.0
  * Rediff patches
  * Add new build dependencies

 -- Ole Streicher <olebole@debian.org>  Mon, 26 Feb 2024 15:10:23 +0100

ginga (4.1.1-1) unstable; urgency=medium

  * New upstream version 4.1.1

 -- Ole Streicher <olebole@debian.org>  Tue, 12 Sep 2023 09:15:02 +0200

ginga (4.1.0-1) unstable; urgency=medium

  * New upstream version 4.1.0

 -- Ole Streicher <olebole@debian.org>  Sat, 01 Jul 2023 18:56:41 +0200

ginga (4.0.1-1) unstable; urgency=medium

  * New upstream version 4.0.1
  * Rediff patches
  * Push Standards-Version to 4.6.2. No changes

 -- Ole Streicher <olebole@debian.org>  Wed, 28 Dec 2022 21:49:45 +0100

ginga (3.4.1-1) unstable; urgency=medium

  * New upstream version 3.4.1
  * Push Standards-Version to 4.6.1. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Fri, 11 Nov 2022 14:40:47 +0100

ginga (3.4.0-1) unstable; urgency=medium

  * New upstream version 3.4.0
  * Rediff patches
  * Adjust dependencies

 -- Ole Streicher <olebole@debian.org>  Tue, 05 Jul 2022 08:16:22 +0200

ginga (3.3.0-1) unstable; urgency=medium

  [ Jenkins ]
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-Browse.
  * Remove obsolete field Name from d/u/metadata

  [ Ole Streicher ]
  * New upstream version 3.3.0
  * Rediff patches
  * Mark test only build options

 -- Ole Streicher <olebole@debian.org>  Sun, 20 Feb 2022 20:47:58 +0100

ginga (3.2.0-2) unstable; urgency=medium

  * Do not depend on non-free Ubuntu mono font
  * Use AUTOPKGTEST_TMP instead of ADTTMP in CI test

 -- Ole Streicher <olebole@debian.org>  Sat, 28 Aug 2021 11:08:02 +0200

ginga (3.2.0-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Fix typo in field name

  [ Ole Streicher ]
  * New upstream version 3.2.0
  * Rediff patches
  * Add new required font ubuntu
  * Push Standards-Version to 4.6.0. No changes needed
  * Ignore runtime warnings during tests
  * Remove conftest.py file

 -- Ole Streicher <olebole@debian.org>  Fri, 27 Aug 2021 11:24:22 +0200

ginga (3.1.0-1) unstable; urgency=low

  * New upstream version 3.1.0. Rediff patches
  * Push Standards-Version to 4.5.0. No changes needed
  * Add Rules-Requires-Root:no to d/control
  * Push dh-compat to 13
  * Update build dependencies. Remove unneeded builddep versions

 -- Ole Streicher <olebole@debian.org>  Fri, 24 Jul 2020 11:43:29 +0200

ginga (3.0.0-1) unstable; urgency=low

  * Add tests on salsa
  * New upstream version 3.0.0. Rediff patches
  * Remove d/compat
  * Add astropy-regions build dependency
  * Explicitly set the version tring for ggrc
  * Switch to pytest for tests

 -- Ole Streicher <olebole@debian.org>  Mon, 23 Sep 2019 12:05:59 +0200

ginga (2.7.2-3) unstable; urgency=medium

  * Fix import failure from astropy.tests on astropy 3.2. Closes: #931998
  * Push Standards-Version to 4.4.0. No changes required.
  * Push compat to 12

 -- Ole Streicher <olebole@debian.org>  Wed, 17 Jul 2019 08:45:43 +0200

ginga (2.7.2-2) unstable; urgency=low

  * Remove Python 2 package

 -- Ole Streicher <olebole@debian.org>  Fri, 14 Dec 2018 15:12:36 +0100

ginga (2.7.2-1) unstable; urgency=low

  * New upstream version 2.7.2. Rediff patches
  * Push Standards-Version to 4.2.1. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Tue, 06 Nov 2018 10:55:00 +0100

ginga (2.7.1-1) unstable; urgency=low

  * New upstream version 2.7.1. Rediff patches
  * Push Standards-Versionto 4.2.0. No changes required
  * Push compat to 11

 -- Ole Streicher <olebole@debian.org>  Thu, 09 Aug 2018 14:45:15 +0200

ginga (2.7.0-2) unstable; urgency=medium

  * Drop python-gtk2 recommendation. (Closes: #885515)
  * Fix font path to /usr/share/fonts/truetype/
  * Adjust example path in generate_cfg_example()

 -- Ole Streicher <olebole@debian.org>  Sat, 17 Mar 2018 13:50:57 +0100

ginga (2.7.0-1) unstable; urgency=low

  * Update VCS fields to use salsa.d.o
  * New upstream version 2.7.0
  * Drop Fix-colormap-test-failures-when-matplotlib-is-installed.patch
  * Push Standards-Version to 4.1.3. Change remaining URLs to https

 -- Ole Streicher <olebole@debian.org>  Sun, 04 Feb 2018 21:02:33 +0100

ginga (2.6.6-2) unstable; urgency=medium

  [Dmitry Shachnev]
  * Update autopkgtests to work with 2.6.6 (that switched to pytest).

  [Pey Lian Lim]
  * Fix colormap test failures when matplotlib is installed

 -- Ole Streicher <olebole@debian.org>  Mon, 20 Nov 2017 21:52:31 +0100

ginga (2.6.6-1) unstable; urgency=low

  * New upstream version 2.6.6
  * Rediff patches
  * Push Standards-Version to 4.1.1. Change d/watch URL to https

 -- Ole Streicher <olebole@debian.org>  Wed, 08 Nov 2017 10:06:34 +0100

ginga (2.6.5-2) unstable; urgency=low

  [Steve Langasek]
  * Fix hard-coded reference to python3.5 in debian/rules. (Closes: #873690)

  [Ole Streicher]
  * Push Standards-Version to 4.1.0. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Wed, 30 Aug 2017 10:41:59 +0200

ginga (2.6.5-1) unstable; urgency=low

  * New upstream version 2.6.5
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Wed, 02 Aug 2017 09:15:47 +0200

ginga (2.6.4-1) unstable; urgency=low

  * New upstream version 2.6.4
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Fri, 30 Jun 2017 12:21:39 +0200

ginga (2.6.2-1) unstable; urgency=medium

  * New upstream version 2.6.2
  * Rediff patches
  * Push Standards-Version to 4.0.0. No changes

 -- Ole Streicher <olebole@debian.org>  Thu, 22 Jun 2017 21:04:06 +0200

ginga (2.6.1-2) unstable; urgency=medium

  * Remove python-webkit from Recommends. Closes: #858948

 -- Ole Streicher <olebole@debian.org>  Wed, 29 Mar 2017 08:51:21 +0200

ginga (2.6.1-1) unstable; urgency=low

  * New upstream version 2.6.1
  * Rediff patches
  * New dependency pyqt

 -- Ole Streicher <olebole@debian.org>  Fri, 23 Dec 2016 15:59:30 +0100

ginga (2.6.0-1) unstable; urgency=low

  * New upstream version 2.6.0
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Thu, 17 Nov 2016 12:28:37 +0100

ginga (2.5.20161108122300-1) unstable; urgency=low

  * New upstream version 2.5.20161108122300
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Wed, 09 Nov 2016 08:29:20 +0100

ginga (2.5.20161005204600-1) unstable; urgency=low

  * New upstream version 2.5.20161005204600

 -- Ole Streicher <olebole@debian.org>  Sat, 08 Oct 2016 13:39:29 +0200

ginga (2.5.20160926130800-1) unstable; urgency=low

  * New upstream version 2.5.20160926130800

 -- Ole Streicher <olebole@debian.org>  Tue, 27 Sep 2016 15:02:41 +0200

ginga (2.5.20160901093400-1) unstable; urgency=low

  * New upstream version 2.5.20160901093400

 -- Ole Streicher <olebole@debian.org>  Fri, 02 Sep 2016 08:56:38 +0200

ginga (2.5.20160810184000-1) unstable; urgency=low

  * Imported Upstream version 2.5.20160810184000

 -- Ole Streicher <olebole@debian.org>  Sun, 14 Aug 2016 20:42:40 +0200

ginga (2.5.20160802122200-1) unstable; urgency=low

  * Imported Upstream version 2.5.20160802122200

 -- Ole Streicher <olebole@debian.org>  Wed, 03 Aug 2016 13:59:50 +0200

ginga (2.5.20160801083400-1) unstable; urgency=low

  * Imported Upstream version 2.5.20160801083400

 -- Ole Streicher <olebole@debian.org>  Tue, 02 Aug 2016 09:15:53 +0200

ginga (2.5.20160627100500-1) unstable; urgency=low

  * New upstream version
  * Re-insert grc as ggrc

 -- Ole Streicher <olebole@debian.org>  Tue, 28 Jun 2016 14:32:55 +0200

ginga (2.5.20160603104100-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Sat, 04 Jun 2016 22:04:18 +0200

ginga (2.5.20160420043855-3) unstable; urgency=low

  * Remove grc binary to solve conflict with the grc package. Closes: #825196

 -- Ole Streicher <olebole@debian.org>  Tue, 24 May 2016 17:14:47 +0200

ginga (2.5.20160420043855-2) unstable; urgency=low

  * Add apache copyright for ginga/util/heaptimer.py
  * Don't use unhinted fonts to avoid conflicts
  * Allow stderr in tests

 -- Ole Streicher <olebole@debian.org>  Mon, 23 May 2016 17:53:26 +0200

ginga (2.5.20160420043855-1) unstable; urgency=low

  * Initial release. (Closes: #823598)

 -- Ole Streicher <olebole@debian.org>  Wed, 18 May 2016 15:11:23 +0200
